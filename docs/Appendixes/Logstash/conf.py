# http://www.sphinx-doc.org/en/master/config
project = 'ROBIN PLATFORM 2.0'
copyright = '2021, ROBIN RPA Team'
author = 'ROBIN RPA Team'

# The short X.Y version
version = ''
# The full version, including alpha/beta/rc tags
release = ''


extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinxcontrib.blockdiag',
    'sphinxcontrib.nwdiag',
    'sphinxcontrib.rackdiag',
    'sphinxcontrib.packetdiag'
]


templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

language = "ru" 

exclude_patterns = []

nitpicky = True # Sphinx will warn about all references where the target cannot be found.
# nitpick_ignore = ('qq','ww')



pygments_style = 'sphinx'

gettext_uuid = True
gettext_compact = False


trim_footnote_reference_space=True 




#html_theme = 'alabaster'
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    'analytics_id': 'UA-XXXXXXX-1',  #  Provided by Google in your dashboard
    'analytics_anonymize_ip': False, 
    'logo_only': True,              # * Только изображение логотипа, не отображать название проекта в верхней части боковой панели
    'display_version': True,        # номер версии отображается в верхней части боковой панели
    'prev_next_buttons_location': 'none',   # расположение кнопок навигации (bottom/top/both/None)
    'style_external_links': True,  # * стилизовать внешние ссылки
    'vcs_pageview_mode': '',       # (display_github/display_gitlab/blob/edit/raw/view/edit)
    'style_nav_header_background': '#ff0000a3', # фон области поиска в навигационном баре
    # Toc options
    'collapse_navigation': True,  # сворачивать или нет подпункты в навигации
    'sticky_navigation': False,    # НЕпрокрутка навигации вместе со страницей
    'navigation_depth': -1,        # максимальная глубина дерева навигации (-1 - бесконечность)
    'includehidden': True,
    'titles_only': True          # * True - подзаголовки не включаются в навигацию
}

html_show_sourcelink = False # запрещаем показ view source


# -- Options for HTMLHelp output ---------------------------------------------
html_show_copyright = True
html_show_sphinx = False

# html_logo= #this must be the name of an image file
# html_favicon = 
html_static_path = ['_static'] # откуда берем стили (css, js)
html_css_files = ['css/theme.css'] # источник стилей
# html_js_files = ['script.js','https://example.com/scripts/custom.js',('custom.js', {'async': 'async'})]

html_compact_lists = True # If true, a list all whose items consist of a single paragraph and/or a sub-list all whose items etc… (recursive definition) will not use the <p> element for any of its items. This is standard docutils behavior 
html_search_language='ru' # язык full text search index


# Output file base name for HTML help builder.
htmlhelp_basename = 'StartPagedoc'



# -- Options for LaTeX output ------------------------------------------------
latex_elements = {
    'classoptions':',openany,oneside',
    'babel':'\\usepackage[russian]{babel}',
    # The paper size ('letterpaper' or 'a4paper').
    #
    'papersize': 'a4paper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'StartPage.tex', 'Конфигурирование Logstash',
     'ROBIN RPA Team', 'manual'),
]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'StartPage', 'ROBIN RPA Documentation',
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'StartPage', 'ROBIN RPA Documentation',
     author, 'ROBIN RPA', 'One line description of project.',
     'Miscellaneous'),
]


# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ''

# A unique identification for the text.
#
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']


# -- Extension configuration -------------------------------------------------

