=====================================================
Конфигурирование filebeat для Linux
=====================================================

.. note:: На примере версии **7.9.2** 
   
- В зависимости от разрядности ОС распаковываем архив **filebeat-7.9.2-linux-x86.tar.gz** или **filebeat-7.9.2-linux-x86_64.tar.gz** в желаемую директорию
- Изменяем атрибуты каталога

 .. code-block:: bash

  sudo chown root filebeat.yml
  sudo chown root modules.d/system.yml

- Изменяем/создаем файл **filebeat.yml** следующего содержания:


.. literalinclude:: ../filebeat.linux.yml
  :language: yaml
  :encoding: utf-8
  :lines: 1-
  :linenos:


- Запускаем для контроля filebeat 

 ..   code-block:: bash

  sudo ./filebeat -e
  
- По успеху прописываем автоматический запуск
